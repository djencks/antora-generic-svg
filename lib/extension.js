'use strict'

const ospath = require('path')
//copied from load-asciidoc
const DOT_RELATIVE_RX = new RegExp(`^\\.{1,2}[/${ospath.sep.replace('/', '').replace('\\', '\\\\')}]`)

module.exports.register = (eventEmitter, config = {}) => {
  if (!config || !config.generator || !config.name) {
    return
  }
  eventEmitter.on('beforeConvertDocuments', ({ playbook, contentCatalog, asciidocConfig }) => {
    let generatorPath = config.generator
    if (generatorPath.charAt() === '.' && DOT_RELATIVE_RX.test(generatorPath)) {
      // NOTE require resolves a dot-relative path relative to current file; resolve relative to playbook dir instead
      generatorPath = ospath.resolve(playbook.dir, generatorPath)
    } else if (!ospath.isAbsolute(generatorPath)) {
      // NOTE appending node_modules prevents require from looking elsewhere before looking in these paths
      const paths = [playbook.dir, ospath.dirname(__dirname)].map((start) => ospath.join(start, 'node_modules'))
      generatorPath = require.resolve(generatorPath, { paths })
    }
    const generator = require(generatorPath)
    const contents = (resourceId, context) => {
      const file = context.contentCatalog.resolveResource(resourceId, context.file.src)
      if (file) {
        return file.contents.toString()
      }
      throw new Error(`resource id ${resourceId} not found from file ${file.src.path}`)
    }
    const register = (registry, context) => doRegister(registry, config.name, generator, context, { contents })
    if (!asciidocConfig.extensions) asciidocConfig.extensions = []
    asciidocConfig.extensions.push({ register })
  })
}

/*
 * Registry is the asciidoctor extension registry, passed in from the calling register method.
 * name is the name of the block to be processed, i.g. [bytefield]
 * processor is the function that will convert the block content to an svg string '<svg xmlns="http://www.w3.org/2000/svg"...'
 *
 */

module.exports.doRegister = doRegister

function doRegister (registry, name, processor, context, fileSource) {
  function toBlock (attrs, parent, source, lineInfo, self) {
    if (typeof attrs === 'object' && '$$smap' in attrs) {
      attrs = fromHash(attrs)
    }
    const doc = parent.getDocument()
    const subs = attrs.subs
    if (subs) {
      source = doc.$apply_subs(attrs.subs, doc.$resolve_subs(subs))
    }
    var svgText
    try {
      svgText = processor(source)
    } catch (err) {
      console.log(`error after ${lineInfo}: ${err.toString()}`)
      svgText = `error after ${lineInfo}: ${err.toString()}`
    }
    const idAttr = attrs.id ? ` id="${attrs.id}"` : ''
    const classAttr = attrs.role ? `${attrs.role} imageblock ${name}` : `imageblock ${name}`
    const titleElement = attrs.title ? `\n<div class="title">${attrs.title}</div>` : ''
    const svgBlock = self.$create_pass_block(
      parent,
      `<div${idAttr} class="${classAttr}">\n<div class="content">${svgText}</div>${titleElement}\n</div>`,
      // eslint-disable-next-line no-undef
      Opal.hash({})
    )
    return svgBlock
  }

  registry.block(name, function () {
    const self = this
    self.onContext(['listing', 'literal'])
    self.process(function (parent, reader, attrs) {
      const lineInfo = reader.$line_info()
      var source = reader.getLines().join('\n')
      return toBlock(attrs, parent, source, lineInfo, self)
    })
  })

  if (fileSource) {
    registry.blockMacro(name, function () {
      const self = this
      self.process((parent, target, attrs) => {
        const lineInfo = parent.document.reader.$line_info()
        target = parent.$apply_subs(target)
        const source = fileSource.contents(target, context)
        return toBlock(attrs, parent, source, lineInfo, self)
      })
    })
  }

  return registry
}

const fromHash = (hash) => {
  const object = {}
  const data = hash.$$smap
  for (const key in data) {
    object[key] = data[key]
  }
  return object
}
