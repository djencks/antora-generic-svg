/* eslint-env mocha */
'use strict'

const { describe } = require('mocha')
const { expect } = require('chai')
const asciidoctor = require('asciidoctor.js')()
const cheerio = require('cheerio')
const test = require('./sample-extension')
const sampleContents = require('./sample-extension').contents
const err = require('./err-extension')

describe('extension tests', () => {
  it('should produce inline svg from block', () => {
    const registry = test.register(asciidoctor.Extensions.create())
    const outputText = asciidoctor.convert(`[sample]
----
This sample text is ignored.
So is this.
----
`, { extension_registry: registry })
    const html = cheerio.load(outputText)
    checkSvg(html)
  })

  it('should produce inline svg from block macro', () => {
    const context = {}
    const fileSource = { contents: sampleContents }
    const registry = test.register(asciidoctor.Extensions.create(), context, fileSource)
    const outputText = asciidoctor.convert('sample::imitation.file[]', { extension_registry: registry })
    const html = cheerio.load(outputText)
    checkSvg(html)
  })

  function checkSvg (html) {
    const svg = html('.sample > div > svg')
    expect(svg.attr('version')).to.equal('1.1')
  }

  it('should explain errors from block', () => {
    const registry = err.register(asciidoctor.Extensions.create())
    const outputText = asciidoctor.convert(`[sample]
----
This sample text is ignored.
So is this.
----
`, { extension_registry: registry })
    expect(outputText).to.contain('error after <stdin>: line 3: Error: Error!')
  })

  it('should explain errors from block macro', () => {
    const context = {}
    const fileSource = { contents: sampleContents }
    const registry = err.register(asciidoctor.Extensions.create(), context, fileSource)
    const outputText = asciidoctor.convert('sample::imitation.file[]', { extension_registry: registry })
    console.log(outputText)
    expect(outputText).to.contain('error after <stdin>: line 2: Error: Error!')
  })
})
