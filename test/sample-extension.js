'use strict'

const extension = require('../lib/extension')

const svg = `<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" contentScriptType="application/ecmascript" contentStyleType="text/css" height="126px" preserveAspectRatio="none" style="width:134px;height:126px;" version="1.1" viewBox="0 0 134 126" width="134px" zoomAndPan="magnify"><defs><filter height="300%" id="f9iteagg1z7n2" width="300%" x="-1" y="-1"><feGaussianBlur result="blurOut" stdDeviation="2.0"/><feColorMatrix in="blurOut" result="blurOut2" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 .4 0"/><feOffset dx="4.0" dy="4.0" in="blurOut2" result="blurOut3"/><feBlend in="SourceGraphic" in2="blurOut3" mode="normal"/></filter></defs><g><line style="stroke: #A80036; stroke-width: 1.0; stroke-dasharray: 5.0,5.0;" x1="32" x2="32" y1="38.2969" y2="86.2969"/><line style="stroke: #A80036; stroke-width: 1.0; stroke-dasharray: 5.0,5.0;" x1="97" x2="97" y1="38.2969" y2="86.2969"/><rect fill="#FEFECE" filter="url(#f9iteagg1z7n2)" height="30.2969" style="stroke: #A80036; stroke-width: 1.5;" width="45" x="8" y="3"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacingAndGlyphs" textLength="31" x="15" y="22.9951">alice</text><rect fill="#FEFECE" filter="url(#f9iteagg1z7n2)" height="30.2969" style="stroke: #A80036; stroke-width: 1.5;" width="45" x="8" y="85.2969"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacingAndGlyphs" textLength="31" x="15" y="105.292">alice</text><rect fill="#FEFECE" filter="url(#f9iteagg1z7n2)" height="30.2969" style="stroke: #A80036; stroke-width: 1.5;" width="56" x="67" y="3"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacingAndGlyphs" textLength="42" x="74" y="22.9951">robert</text><rect fill="#FEFECE" filter="url(#f9iteagg1z7n2)" height="30.2969" style="stroke: #A80036; stroke-width: 1.5;" width="56" x="67" y="85.2969"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacingAndGlyphs" textLength="42" x="74" y="105.292">robert</text><polygon fill="#A80036" points="85,50.2969,95,54.2969,85,58.2969,89,54.2969" style="stroke: #A80036; stroke-width: 1.0;"/><line style="stroke: #A80036; stroke-width: 1.0;" x1="32.5" x2="91" y1="54.2969" y2="54.2969"/><polygon fill="#A80036" points="43.5,64.2969,33.5,68.2969,43.5,72.2969,39.5,68.2969" style="stroke: #A80036; stroke-width: 1.0;"/><line style="stroke: #A80036; stroke-width: 1.0;" x1="37.5" x2="96" y1="68.2969" y2="68.2969"/><!--MD5=[04bffa01f7c1857105c3b29278406799]
@startuml
alice -> robert
robert -> alice
@enduml

PlantUML version 1.2020.01beta5(Unknown compile time)
(GPL source distribution)
Java Runtime: Java(TM) SE Runtime Environment
JVM: Java HotSpot(TM) 64-Bit Server VM
Java Version: 1.7.0_25-b15
Operating System: Linux
Default Encoding: UTF-8
Language: en
Country: US
--></g></svg>`

module.exports.register = (registry, context = null, filesource = null) =>
  extension.doRegister(registry, 'sample', (lines) => svg, context, filesource)

module.exports.contents = (file) => svg
